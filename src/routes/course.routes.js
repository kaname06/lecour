const { Router } = require('express')
const { authorize, ADMIN} = require('../utils/auth')
const controller = require('../controllers/course.controller')
const router = Router()

router
    .route('/')
    .get(controller.getAllActive)
    .post(authorize(ADMIN), controller.store)

router
    .route('/temary/:courseKey')
    .get(authorize(), controller.getTemary)

router
    .route('/one/:key')
    .get(controller.getOneActive)

router
    .route('/one/desactivate/:coursekey')
    .get(authorize(ADMIN), controller.desactivateCourse)

router
    .route('/admin/one/:key')
    .get(authorize(ADMIN), controller.getOne)

router
    .route('/admin')
    .get(authorize(ADMIN), controller.getAll)

router
    .route('/admin/module')
    .post(authorize(ADMIN), controller.addModule)

router
    .route('/admin/class/classId?')
    .post(authorize(ADMIN), controller.addClass)
    .delete(authorize(ADMIN), controller.desactivateClass)
    
module.exports = router