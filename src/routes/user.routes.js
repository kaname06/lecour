const controller = require('../controllers/user.controller')
const { Router } = require('express')
const { authorize, ADMIN, LOGGED_USER } = require('../utils/auth');
let router = Router()

router
    .route('/')
    .get(authorize(ADMIN), controller.getAll)
    .post(controller.store)

router
    .route('/create')
    .post(controller.store)

router
    .route('/one/:user')
    .get(authorize(ADMIN), controller.getOne)

router
    .route('/me')
    .get(authorize(), controller.getUser)

router
    .route('/auth')
    .post(controller.login)

router
    .route('/update')
    .post(authorize(LOGGED_USER), controller.SelfUpdate)

router
    .route('/update/password')
    .put(authorize(LOGGED_USER), controller.updatePassword)

router
    .route('/update/:user')
    .post(authorize(ADMIN), controller.update)

router
    .route('/contact')
    .post(controller.contact)

router
    .route('/restore/password/:email?')
    .get(controller.getRestorePasswordUrl)
    .post(controller.restorePassword)

router
    .route('/restore/password/verify/:user/:token')
    .get(controller.verifyPasswordRestore)

router
    .route('/activate/course')
    .post(authorize(ADMIN), controller.activateCourse)

module.exports = router
