const { Router } = require('express')
const { authorize, ADMIN} = require('../utils/auth')
const controller = require('../controllers/purchase.controller')
const router = Router()

router
    .route('/')
    .get(authorize(ADMIN), controller.getAll)
    .post(authorize(), controller.store)

router
    .route('/mine')
    .get(authorize(), controller.getMyPurchases)

router
    .route('/confirm')
    .post(controller.confirm)

router
    .route('/cart')
    .post(authorize(), controller.storeCart)

module.exports = router