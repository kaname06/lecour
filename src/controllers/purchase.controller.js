const Purchase = require('../models/purchase.model')
const Cart = require('../models/cart.model')
const User = require('../models/user.model')

exports.store = async (req, res, next) => {
    try {
        let data = new Purchase(req.body)
        var error = data.validateSync();
        if (error) {
            let status = []
            for (const key in error.errors) {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi, "")
                };
                status.push(res);
            }
            return res.status(400).json({ success: false, info: 'Invalid data structure', data: status })
        } else {
            let status = await data.save();
            if (!('_id' in status)) {
                return res.status(400).json({ success: false, info: 'Fatal Error, unable to store Purchase, try later' })
            } else {
                return res.status(200).json({ success: true, info: 'Purchase save successfully', data: status })
            }
        }
    } catch (error) {
        next(error)
    }
}

exports.storeCart = async (req, res, next) => {
    try {
        let data = new Cart(req.body)
        var error = data.validateSync();
        if (error) {
            let status = []
            for (const key in error.errors) {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi, "")
                };
                status.push(res);
            }
            return res.status(400).json({ success: false, info: 'Invalid data structure', data: status })
        } else {
            let status = await data.save();
            if (!('_id' in status)) {
                return res.status(400).json({ success: false, info: 'Fatal Error, unable to store Cart, try later' })
            } else {
                return res.status(200).json({ success: true, info: 'Cart save successfully', data: status })
            }
        }
    } catch (error) {
        next(error)
    }
}

exports.getMyPurchases = async (req, res, next) => {
    try {
        const data = await Purchase.find({user: req.user._id}).populate({
            path: 'cart',
            populate: {
                path: 'items.course'
            } 
        })
        return res.status(200).json({success: true, info: 'Query do it successfully', data: data})
    } catch (error) {
        next(error)
    }
}

exports.getAll = async (req, res, next) => {
    try {
        const data = await Purchase.find().populate({
            path: 'cart',
            populate: {
                path: 'items.course'
            } 
        }).populate('user')
        return res.status(200).json({success: true, info: 'Query do it successfully', data: data})
    } catch (error) {
        next(error)
    }
}

exports.confirm = async (req, res, next) => {
    try {
        console.log(req.body)
        const { state_pol, reference_sale } = req.body
        const purchase = await (await Purchase.findById(reference_sale)).populate('cart')
        if (purchase && purchase._id) {
            if(state_pol === '4') {
                purchase.state = 'approved'

                const user = await User.findById(purchase.user)
                if (purchase.cart && purchase.cart.items) {
                    for (const item of purchase.cart.items) {
                        user.courses.push({
                            course: item.course,
                            purchase: reference_sale
                        })
                    }
                    await user.save()
                }
            }
            else {
                purchase.state = "declined"
            }
            
            purchase.transaction = req.body
            await purchase.save()
        }
        return res.status(200).send('ok')
    } catch (error) {
        next(error)
    }
}