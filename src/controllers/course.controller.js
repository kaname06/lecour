const { Types } = require('mongoose');
const Course = require('../models/course.model')
const Class = require('../models/class.model')

exports.store = async (req, res, next) => {
    try {
        let data = new Course(req.body)
        var error = data.validateSync();
        if (error) {
            let status = []
            for (const key in error.errors) {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi, "")
                };
                status.push(res);
            }
            return res.status(400).json({ success: false, info: 'Invalid data structure', data: status })
        } else {
            let status = await data.save();
            if (!('_id' in status)) {
                return res.status(400).json({ success: false, info: 'Fatal Error, unable to store Course, try later' })
            } else {
                return res.status(200).json({ success: true, info: 'Course save successfully', data: status })
            }
        }
    } catch (error) {
        next(error)
    }
}

exports.desactivateCourse = async (req, res, next) => {
    try {
        const { courseKey } = req.params
        if (!courseKey || courseKey == '')
            return res.status(400).json({success: false, info: 'Invalid data structure'})
        const data = await Course.findOneAndUpdate({key: courseKey}, {status: false}, {new: true})
        if (!data || !data._id)
            return res.status(400).json({success: false, info: 'Internal server error'})
        return res.status(200).json({success: true, info: 'Course desactivated successfully'})
    } catch (error) {
        next(error)
    }
}

exports.getOneActive = async (req, res, next) => {
    try {
        const { key } = req.params
        const data = await Course.findOne({key, status: true})
        if (!data || data._id)
            return res.status(400).json({success: false, info: 'Course not found'})
        return res.status(200).json({success: true, info: 'Query do it successfully', data: data})
    } catch (error) {
        next(error)
    }
}

exports.getOne = async (req, res, next) => {
    try {
        const { key } = req.params
        const data = await Course.findOne({key, status: true})
        if (!data || data._id)
            return res.status(400).json({success: false, info: 'Course not found'})
        await data.getAllClasses()
        return res.status(200).json({success: true, info: 'Query do it successfully', data: data})
    } catch (error) {
        next(error)
    }
}

exports.getAllActive = async (req, res, next) => {
    try {
        const data = await Course.find({status: true}).sort({order: -1})
        return res.status(200).json({success: true, info: 'Query do it successfully', data: data})
    } catch (error) {
        next(error)
    }
}

exports.getAll = async (req, res, next) => {
    try {
        const data = await Course.find()
        return res.status(200).json({success: true, info: 'Query do it successfully', data: data})
    } catch (error) {
        next(error)
    }
}

exports.getTemary = async (req, res, next) => {
    try {
        const { courseKey } = req.params
        if (!courseKey || courseKey == '')
            return res.status(400).json({success: false, info: 'Invalid data structure'})
        const data = await Course.findOne({key: courseKey})
        if (!data || !data._id)
            return res.status(400).json({success: false, info: 'Course not found'})
        if(!req.user.hasCourse(data._id))
            return res.status(400).json({success: false, info: 'You dont have this course'})
        return res.status(200).json({success: true, info: 'Query do it successfully', data: data.transform()})
    } catch (error) {
        next(error)
    }
}

exports.addModule = async (req, res, next) => {
    try {
        const {course, module} = req.body
        if (!course || !Types.ObjectId.isValid(course) || !module || module == '')
            return res.status(400).json({success: false, info: 'Invalid data structure'})
        const data = await Course.findByIdAndUpdate(course, {$push: {modules: {name: module}}}, {new: true})
        if (!data || !data._id)
            return res.status(400).json({success: false, info: 'Internal server error'})
        return res.status(200).json({success: true, info: 'Query do it successfully', data: data})
    } catch (error) {
        next(error)
    }
}

exports.addClass = async (req, res, next) => {
    try {
        let data = new Class(req.body)
        var error = data.validateSync();
        if (error) {
            let status = []
            for (const key in error.errors) {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi, "")
                };
                status.push(res);
            }
            return res.status(400).json({ success: false, info: 'Invalid data structure', data: status })
        } else {
            let status = await data.save();
            if (!('_id' in status)) {
                return res.status(400).json({ success: false, info: 'Fatal Error, unable to store Class, try later' })
            } else {
                return res.status(200).json({ success: true, info: 'Class save successfully', data: status })
            }
        }
    } catch (error) {
        next(error)
    }
}

exports.desactivateClass = async (req, res, next) => {
    try {
        const { classId } = req.params
        if (!classId || !Types.ObjectId.isValid(classId))
            return res.status(400).json({success: false, info: 'Invalid data structure'})
        const data = await Class.findByIdAndUpdate(classId, {status: false}, {new: true})
        if (!data || !data._id)
            return res.status(400).json({success: false, info: 'Internal server error'})
        return res.status(200).json({success: true, info: 'Class desactivated successfully'})
    } catch (error) {
        next(error)
    }
}