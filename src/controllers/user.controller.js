const { Types } = require('mongoose');
const { contactTarget, clientUrl } = require('../config/vars');
const User = require('../models/user.model')
const SendEmail = require('../utils/email')
const randomstring = require('randomstring')

exports.store = async (req, res, next) => {
    try {
        let vali = await User.findOne({ email: req.body.email })
        if (vali && vali._id)
            return res.status(400).json({ success: false, info: 'Email is already registered' })
        let data = new User(req.body)
        var error = data.validateSync();
        if (error) {
            let status = []
            for (const key in error.errors) {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi, "")
                };
                status.push(res);
            }
            return res.status(400).json({ success: false, info: 'Invalid data structure', data: status })
        } else {
            let status = await data.save();
            if (!('_id' in status)) {
                return res.status(400).json({ success: false, info: 'Fatal Error, unable to store User, try later' })
            } else {
                return res.status(200).json({ success: true, info: 'User save successfully', data: status })
            }
        }
    } catch (error) {
        next(error)
    }
}

exports.getUser = async (req, res, next) => {
    try {
        const data = await User.findById(req.user._id, { password: 0, _v: 0, role: 0 }).populate('courses.course')
        if (!data || !data._id)
            return res.status(400).json({ success: false, info: "User not found" })
        return res.status(200).json({ success: true, info: "User found", data })
    } catch (error) {
        next(error)
    }
}

exports.getOne = async (req, res, next) => {
    try {
        const { user } = req.params
        if (!user || !Types.ObjectId.isValid(user))
            return res.status(400).json({ success: false, info: 'invalid data structure' })
        const data = await User.findById(user)
        return res.status(200).json({ success: true, info: 'Ok', data: data })
    } catch (error) {
        next(error)
    }
}

exports.getAll = async (req, res, next) => {
    try {
        const data = await User.find({ role: 'user' }, { role: 0, password: 0 }).sort({ createdAt: 1 })
        return res.status(200).json({ success: false, info: "Query do it successfully", data })
    } catch (error) {
        next(error)
    }
}

exports.login = async (req, res, next) => {
    try {
        const resp = await User.findAndGenerateToken(req.body);
        if (resp == null)
            return res.status(400).json({ success: false, info: "Credentials not found" })
        else if (!resp.success)
            return res.status(400).json(resp)
        const { user, token } = resp
        return res.status(200).json({ success: true, token, user });
    } catch (error) {
        return next(error);
    }
}

exports.updatePassword = async (req, res, next) => {
    try {
        const { old, password } = req.body
        if (!password || !old)
            return res.status(400).json({ success: false, info: 'Invalid data structure' })
        let data = await User.findById(req.user._id)
        if (!data || !data._id || !(await data.passwordMatches(old)))
            return res.status(400).json({ success: false, info: 'User not found' })
        data.password = password
        let Data = await data.save()
        if (!Data || !Data._id)
            return res.status(400).json({ success: false, info: 'Internal server error' })
        return res.status(200).json({ success: true, info: 'Password updated successfully' })
    } catch (error) {
        next(error)
    }
}

exports.getRestorePasswordUrl = async (req, res, next) => {
    try {
        const { email } = req.params
        if (!email || email == '')
            return res.status(400).json({success: false, info: 'Invalid data structure'})
        const user = await User.findOne({email})
        if (!user || !user._id)
            return res.status(400).json({success: false, info: 'User not found'})
        let token = randomstring.generate({
            length: 8,
            charset: 'numeric'
        });
        user.securityToken = token
        let sav = await user.save()
        if (!sav || !sav._id)
            return res.status(400).json({success: false, info: 'Internal error'})
        let restoreUrl = `${clientUrl}/password/${user._id}/${token}`
        SendEmail(user.email, "Recuperar contraseña", "resetpassword", {url: restoreUrl, user})
        return res.status(200).json({success: true, info: 'Email sended successfully'})
    } catch (error) {
        next(error)
    }
}

exports.verifyPasswordRestore = async (req, res, next) => {
    try {
        const { user, token } = req.params
        if (!user || !token || !Types.ObjectId.isValid(user) || token == '')
            return res.status(400).json({success: false, info: 'Invalid data structure'})
        const data = await User.findOne({_id: user, securityToken: token})
        if (!data || !data._id)
            return res.status(400).json({success: false, info: 'Invalid token'})
        return res.status(200).json({success: true, info: 'Correct info'})
    } catch (error) {
        next(error)
    }
}

exports.restorePassword = async (req, res, next) => {
    try {
        const { user, password } = req.body
        if (!user || !Types.ObjectId.isValid(user) || !password || password == '')
            return res.status(400).json({success: false, info: 'Invalid data structure'})
        const data = await User.findById(user)
        if (!data || !data._id)
            return res.status(400).json({success: false, info: 'User not found'})
        data.password = password
        data.securityToken = null
        let vali = await data.save()
        if (!vali || !vali._id)
            return res.status(400).json({success: false, info: 'Internal server error'})
        return res.status(200).json({success: true, info: 'Password updated successfully'})
    } catch (error) {
        next(error)
    }
}

exports.SelfUpdate = async (req, res, next) => {
    try {
        let data = await User.findByIdAndUpdate(req.user._id, req.body, { new: true })
        if (!data || !data._id)
            return res.status(400).json({ success: false, info: "Someting went wrong", data })
        return res.status(200).json({ success: true, info: "Information updated Successfully" })
    } catch (error) {
        next(error)
    }
}

exports.update = async (req, res, next) => {
    try {
        const { user } = req.params
        if (!user || !Types.ObjectId.isValid(user) || !req.body)
            return res.status(400).json({ success: false, info: "Invalid data structure" })
        let data = await User.findByIdAndUpdate(user, req.body, { new: true })
        if (!data || !data._id)
            return res.status(400).json({ success: false, info: "User not found" })
        return res.status(200).json({ success: false, info: "User updated successfully" })
    } catch (error) {
        next(error)
    }
}

exports.contact = async (req, res, next) => {
    try {
        const { email, message, name } = req.body
        if (!email || email == "" || !message || message == "" || !name || name == "")
            return res.status(400).json({ success: false, info: 'Invalid data structure' })
        SendEmail(contactTarget, "Contacto desde el sitio web", "contact", { message, name })
        return res.status(200).json({ success: true, info: 'email sended successfully' })
    } catch (error) {
        next(error)
    }
}

exports.activateCourse = async (req, res, next) => {
    try {
        const {user, course} = req.body
        if (!user || !Types.ObjectId.isValid(user) || !course || !Types.ObjectId.isValid(course))
            return res.status(400).json({success: false, info: 'Invalid data structure'})
        const eluser = await User.findById(user)
        if (!eluser || !eluser._id)
            return res.status(400).json({success: false, info: 'User not found'})
        let vali = eluser.courses.some(cour => cour.course.toString() === course.toString())
        if (vali)
            return res.status(400).json({success: false, info: 'User already have this course'})
        eluser.courses.push({
            course,
            purchase: 'Activated by admin'
        })
        let save = await eluser.save()
        if (!save || !save._id)
            return res.status(400).json({success: false, info: 'Internal server error'})
        return res.status(200).json({success: true, info: 'Course activated successfully'})
    } catch (error) {
        next(error)
    }
}