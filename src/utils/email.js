const mailGun = require('mailgun-js')
const Email = require('email-templates')

const { apiKeyMailGun, domainMailGun, clientUrl } = require('../config/vars')

const renderViewEmail = new Email()
const mg = mailGun({ apiKey: apiKeyMailGun, domain: domainMailGun })

module.exports = (to, subject, view, data, from = 'Lecour <no-reply@lecour.co>') => {
    renderViewEmail
      .render(view, {...data, ClientUrl: clientUrl})
      .then(html => {
        mg.messages().send({ from, to, subject, html }, (error, body) => {
          if (error) console.error(`Error sending message of: ${subject}, to: ${to}`, error);
          else console.log(`Message of ${subject} sended to: ${to}, id: ${body.id}`)
        });
      })
      .catch(console.error)
  }

