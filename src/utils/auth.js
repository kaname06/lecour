const passport = require('passport');
// const User = require('../models/user.model');

const ADMIN = 'admin';
const LOGGED_USER = '_loggedUser';

const Roles = ["user", 'admin']

const handleJWT = (req, res, next, roles) => async (err, user, info) => {
  const error = err || info;
  if (!user) {
    return next('Unauthorized')
  }
  if (roles === LOGGED_USER) {
    if (user.role == 'admin') {
      return next('ur admin');
    }
  } else if (roles === ADMIN) {
    if (user.role == 'user') {
      return next('ur user')
    }
  } else if (!Roles.includes(user.role)) {
    return next('notfound');
  } else if (error|| !user) {
    return next('nouser');
  }
  
  req.user = user;

  return next();
};

exports.ADMIN = ADMIN;
exports.LOGGED_USER = LOGGED_USER;

exports.authorize = (roles = Roles) => (req, res, next) =>
  passport.authenticate(
    'jwt', { session: false },
    handleJWT(req, res, next, roles),
  )(req, res, next);
