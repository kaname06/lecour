require('dotenv').config()
const app = require('./config/app')
const { port } = require('./config/vars')

require('./config/database')
app.listen(port, () => console.log(`Server running on port: ${port}`))
