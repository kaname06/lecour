module.exports = {
    port: process.env.PORT || 3000,
    allowedOrigins: [],
    mongoUri: process.env.MONGODB_URI || 'mongodb://localhost:27017/lecour',
    jwtSecret: process.env.JWT_SECRET || 'm1sup3rs3cr3t',
    apiKeyMailGun: process.env.MAILGUN_APY_KEY || '123', 
    domainMailGun: process.env.MAILGUN_DOMAIN || 'mg.lecour.com', 
    clientUrl: process.env.CLIENT_URL || 'lecour.com'
}