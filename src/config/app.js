const express = require('express')
const cors = require('cors')
const morgan = require('morgan')
const helmet = require('helmet')
const passport = require('passport')
const PassportStrategies = require('./auth')

const { allowedOrigins } = require('./vars')

const app = express()

//imports
const UserRoutes = require('../routes/user.routes')
const CourseRoutes = require('../routes/course.routes')
const PurchaseRoutes = require('../routes/purchase.routes')

//Middlewares
app.use((req, res, next) => {
    res.setHeader('X-Developed-By', 'Adrian Correa <kaname06>')
    next()
})
if (process.env.NODE_ENV == 'production') {
    console.log('Running in prod mode')
    app.use(cors({
        origin: allowedOrigins
    }))
    
    app.use(helmet())
}
else {
    console.log('Running in dev mode')
    app.use(cors())
    app.use(morgan('dev'))
}
app.use(express.json())
app.set('trust proxy', true)
app.use(passport.initialize())

passport.use('jwt', PassportStrategies.jwt)

//Routes
app.use('/user', UserRoutes)
app.use('/course', CourseRoutes)
app.use('/purchase', PurchaseRoutes)

//Not found
app.use((req, res, next) => {
    return res.status(400).json({success: false, info: `Unable to ${req.method} this route: ${req.path}`})
})

module.exports = app