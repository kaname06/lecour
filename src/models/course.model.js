const { Schema, model } = require('mongoose')
const Class = require('./class.model')

const Course = new Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    banner: {
        type: String,
        required: true
    },
    card: {
        type: String,
    },
    icon: {
        type: String
    },
    key: {
        type: String,
        unique: true,
        required: true
    },
    order: {
        type: Number
    },
    status: {
        type: Boolean,
        default: true
    },
    descriptions: {
        short: {
            type: String,
            required: true
        },
        large: {
            type: String,
            default: null
        }
    },
    modules: [{
        name: {
            type: String
        }
    }],
    price: [{
        currency: {
            type: String,
            enum: ['USD', 'COP'],
            required: true
        },
        amount: {
            type: Number,
            required: true
        },
        discount: {
            type: Number,
            min: 0,
            max: 1
        }
    }],
    teachers: [{
        name: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        photo: {
            type: String,
            required: true
        }
    }],
    toLearn: [{
        content: {
            type: String,
            required: true
        }
    }]
}, {
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
})

Course.virtual('classes')

Course.methods = {
    async getClasses() {
        const data = await Class.find({course: this._id, status: true}, {course: 0, videoUrl: 0})
        this.set('classes', data)
    },
    async getAllClasses() {
        const data = await Class.find({course: this._id}, {course: 0, videoUrl: 0})
        this.set('classes', data)
    },
    transform() {
        return {
            name: this.name,
            key: this.key,
            icon: this.icon,
            descriptions: this.descriptions,
            modules: this.modules,
            classes: this.classes,
            teachers: this.teachers,
            toLearn: this.toLearn
        }
    }
}

Course.post(['findById', 'findOne'], async function(result) {
    if (result) {
        await result.getClasses()
    }
})

module.exports = model('Course', Course)