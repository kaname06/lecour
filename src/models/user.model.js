const { Schema, model } = require('mongoose')
const bcrypt = require('bcryptjs')

const User = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    courses: [{
        course: {
            type: Schema.Types.ObjectId,
            ref: 'Course',
            required: true
        },
        active: {
            type: Boolean,
            default: true
        },
        purchase: {
            type: Schema.Types.Mixed,
            default: null
        }
    }],
    password: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

User.virtual('myCourses')

User.pre('save', async function save(next) {
    try {
      const rounds = process.env.NODE_ENV === 'development' ? 1 : 10;
      const hash = bcrypt.hashSync(this.password, bcrypt.genSaltSync(rounds))
      this.password = hash;
  
      return next();
    } catch (error) {
      return next(error);
    }
})

User.methods = {
    token() {
        const payload = {
          // exp: moment().add(jwtExpirationInterval, 'minutes').unix(),
        //   iat: moment().unix(),
          sub: this._id,
        };
        return jwt.sign(payload, jwtSecret);
    },
    passwordMatches(password) {
        return bcrypt.compare(password, this.password);
    },
    hasCourse(course) {
        return this.courses.some(cour => cour.course.toString() === course.toString() && cour.active == true)
    }
}

User.statics = {
    async findAndGenerateToken(options) {
        const { email, password, type } = options;
        if (!email || !type) return null;
    
        const user = await this.findOne({ email }).exec();
        const err = {
          success: false
        };
        if (password) {
            let userType = type == 1 ? 'user' : 'admin'
          if (user && await user.passwordMatches(password) && user.role == userType) {
            return { success: true, user, token: user.token() };
          }
          err.info = 'Correo o contraseña incorrecta';
        }
        return err;
    }
}

module.exports = model('User', User)