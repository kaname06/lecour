const { Schema, model } = require('mongoose')

const Cart = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    currency: {
        type: String,
        enum: ['USD', 'COP'],
        default: 'USD'
    },
    totalAmount: {
        type: Number,
        required: true
    },
    items: [{
        course: {
            type: Schema.Types.ObjectId,
            ref: 'Course'
        },
        amount: {
            type: Number,
            required: true
        }
    }]
}, {
    timestamps: true
})

module.exports = model('Cart', Cart)