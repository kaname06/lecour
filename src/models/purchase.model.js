const { Schema, model } = require('mongoose')

const Purchase = new Schema({
    amount: {
        type: Number,
        required: true
    },
    currency: {
        type: String,
        enum: ['USD', 'COP'],
        default: 'COP'
    },
    paymentMethod: {
        type: String,
        enum: ['payu', 'cash', 'transfer'],
        default: 'payu'
    },
    user: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    cart: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Cart'
    },
    state: {
        type: String,
        enum: ['pending', 'declined', 'approved'],
        default: 'pending'
    },
    transaction: {
        type: Schema.Types.Mixed
    }
}, {
    timestamps: true
})

module.exports = model('Purchase', Purchase)